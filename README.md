Various script used on [Webarchitects](https://www.webarchitects.coop/) servers.

Install
=======

Check out the repo and run the `install.sh` script to copy the files in `bin/` to /`usr/local/bin`:

```bash
sudo -i
cd /usr/local/src
git clone https://git.coop/webarch/scripts.git
cd scripts
bash ./install.sh
```

Or use Ansible &mdash; this repo is a Galaxy role.
