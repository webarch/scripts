#!/usr/bin/env bash

GIT_PULL=$(git pull)
if [[ "${GIT_PULL}" != "Already up to date." ]]; then
  bash ./install.sh
fi
