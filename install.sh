#!/usr/bin/env bash

SRC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/bin"
DEST="/usr/local/bin"

# Check that the script is being run by root
if [[ "$(id -u)" != "0" ]] ; then
  echo "You must run '$0' as root or via sudo" 
  exit 1
fi

if [[ -d "${SRC}" ]]; then
  if [[ -d "${DEST}" ]]; then
    # Copy files to $DEST
    cp -a "${SRC}"/* "${DEST}" && echo "Scripts installed" || { echo "problem installing scripts" ; exit 1 ; }
  else
    echo "${DEST} doesn't appear to exist? Please create it and check the permissions"
    exit 1
  fi
else
  echo "${SRC} doesn't appear to exist?"
  exit 1
fi

exit 0
