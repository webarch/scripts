#!/usr/bin/env bash

set -e

DOWNLOAD_DIR="/usr/local/src"
if [[ ! -d ${DOWNLOAD_DIR} ]]; then
  mkdir -p "${DOWNLOAD_DIR}"
fi

LATEST_VERSION=$(curl -sI https://github.com/docker/compose/releases/latest | grep ^location | sed 's;^location: https:/;;' | sed 's/\r$//' | awk -F"/" '{print $NF}') 
KERNEL=$(uname -s)
MACHINE=$(uname -m)
LATEST_VERSION_URL="https://github.com/docker/compose/releases/download/${LATEST_VERSION}/docker-compose-${KERNEL}-${MACHINE}"

# Download the latest version and the checksum
curl -sL "${LATEST_VERSION_URL}" -o "${DOWNLOAD_DIR}/docker-compose.${LATEST_VERSION}" || { echo "Error downloading Docker Compose" ; exit 1 ; }
curl -sL "${LATEST_VERSION_URL}.sha256" -o "${DOWNLOAD_DIR}/docker-compose.${LATEST_VERSION}.sha256" || { echo "Error downloading Docker Compose checksum" ; exit 1 ; }

# Check the checksum
if [[ -f "${DOWNLOAD_DIR}/docker-compose.${LATEST_VERSION}.sha256" ]]; then
  grep $( sha256sum ${DOWNLOAD_DIR}/docker-compose.${LATEST_VERSION} | awk '{ print $1 }' ) ${DOWNLOAD_DIR}/docker-compose.${LATEST_VERSION}.sha256 || { echo "Checksum didn't match" ; exit 1 ; }
  mv "${DOWNLOAD_DIR}/docker-compose.${LATEST_VERSION}" /usr/local/bin/docker-compose && chmod 755 /usr/local/bin/docker-compose && { echo "Docker Compose ${LATEST_VERSION} installed" ; exit 0 ; }
else
  echo "Something went wrong"
  exit 1
fi

