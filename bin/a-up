#!/usr/bin/env bash

# This script runs "aptitude full-upgrade" and updates the Changelog

# location of the logchange script, we assume it has been
# installed int he same directory as this script is in
#DIR="/usr/local/bin"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOGCHANGE="$DIR/logchange"

# check that the script is being run by root
if [[ "$(id -u)" != "0" ]] ; then
  echo "You must run '$0' as root or via sudo"
  exit 1
fi

# check for -y on standard input
ASSUME_YES=""
if [[ "${1}" == "-y" ]]; then
  ASSUME_YES="--assume-yes"
fi

# check that the logchange script is installed
if [[ ! -f "${LOGCHANGE}" ]] ; then
  echo "You need to install the '${LOGCHANGE}' script before you can run $0"
  exit 2
fi

# check that packages are installed and if not then install, this depends on the
# package name matching the binary in /usr/bin, this check is done first because
# it's faster
DEPENDENCIES="apt-show-versions aptitude"
for d in ${DEPENDENCIES}; do
  if [[ ! -f "/usr/bin/${d}" ]] ; then
    DEP_CHECK=$(dpkg -s ${d} 2>&1 | grep "Status: install ok installed")
    if [[ ! "${DEP_CHECK}" ]]; then
       echo "Installing '${d}'"
       apt-get install ${d}
    fi
  fi
done

# get updates
apt-get -qq update
# get a list of updates
UPDATES=$(apt-show-versions -b -u | xargs)
# if we have updates then install them and write the list of updates to the
# Changelog
if [[ "${UPDATES}" ]]; then
  echo "About to upgrade '${UPDATES}'"
  ${LOGCHANGE} "${UPDATES} : updated"
  aptitude ${ASSUME_YES} full-upgrade
  # Delete dowloaded packages to free up space
  apt clean
  # Remove packages that are now no longer needed
  apt-get autoremove
  # make sure apt munin plugin updates it's state
  if [[ -f "/var/lib/munin-node/plugin-state/nobody/plugin-apt.state" ]]; then
    rm -f /var/lib/munin-node/plugin-state/nobody/plugin-apt.state
    munin-run apt_all
  fi
  # make sure apt_all munin plugin updates it's state
  if [[ -f "/var/lib/munin-node/plugin-state/nobody/plugin-apt_all.state" ]]; then
    rm -f /var/lib/munin-node/plugin-state/nobody/plugin-apt_all.state
    munin-run apt_all
  fi
  exit 0
else
  echo "No updates today"
  exit 0
fi

