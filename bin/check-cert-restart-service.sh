#!/usr/bin/env bash

set -eE

jc_min_version="1.22.0"

# Check that jc is available
if ! command -v jc &> /dev/null
then
  echo "jc could not be found"
  exit 1
else
  jc_version=$(jc -v | head -n1 | awk '{ print $3 }')
  if [[ "${jc_version}" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    jc_version_check=$(printf "${jc_version}\n${jc_min_version}" | sort -V | head -n1)
    if [[ "${jc_version_check}" != "${jc_min_version}" ]]; then
      echo "jc version ${jc_min_version} is the minimum version required"
      exit 1
    fi
  else
    echo "There was an error parsing jc -v"
    exit 1
  fi
fi

# Check that the server is using UTC
localtime=$(readlink -f /etc/localtime)
tz=$(basename "${localtime}")
if [[ "${tz}" != "UTC" && "${tz}" != "UCT" ]]; then
  echo "The clock needs to be set to UTC"
  exit 1
fi

# Check that jp is available
if ! command -v jp &> /dev/null
then
  echo "jp could not be found"
  exit 1
fi

# Check that jq is available
if ! command -v jq &> /dev/null
then
  echo "jq could not be found"
  exit 1
fi

# Check that openssl is available
if ! command -v openssl &> /dev/null
then
  echo "openssl could not be found"
  exit 1
fi

# Check that a cert path has been provided as the first argument
if [[ ! "${1}" ]]; then
  hostname=$(hostname -f)
  echo "Please supply a path to a cert as the first argument and a service as the second argument"
  echo "For example /etc/ssl/le/${hostname}.fullchain.pem apache2"
  exit 1
else
  if [[ ! -f "${1}" ]]; then
    echo "No file found at ${1}"
    exit 1
  else
    # Check that it is a x509 cert
    openssl=$(openssl x509 -noout -text -in "${1}" | head -n1)
    if [[ "${openssl}" == "Certificate:" ]]; then
      cert="${1}"
    else
      echo "No certificate found at ${1}"
      exit 1
    fi
  fi
fi

# Check that a service name has been provided as the second argument
if [[ ! "${2}" ]]; then
  echo "Please supply a service name as the second argument"
  echo "For example ${0} ${cert} apache2"
  exit 1
else
  # Check that the service exists
  service=$(systemctl | jc -q -m --systemctl | jq -r . | jp -c -u "[?unit=='${2}.service'].unit | [0]" | sed 's/\.service$//')
  if [[ "${2}" != "${service}" ]]; then
    echo "No ${2} service appears to be running, so it can't be restarted"
    exit 1
  fi
fi

# Get the date the cert was created in UNIX time
created=$(jc -q -m --x509-cert < "${cert}" | jq -r . | jp -c -u '[].tbs_certificate.validity.not_before | [0]')
# Check that the cert created date is a positive number
if [[ "${created}" == +([0-9]) ]]; then
  # Get the service started date
  started=$(systemctl show "${service}" | jc -q -m --ini | jq -r . | jp -c -u ActiveEnterTimestamp | jc -q -m --date | jq -r . | jp -c -u epoch)
  # Check that the service started date is a positive number
  if [[ "${started}" == +([0-9]) ]]; then
    # Check if the created date is more recent than the started date
    if (( created > started )); then
      # Check that the script is being run by root
      if [[ "$(id -u)" == "0" ]] ; then
        # Restart the service
        service "${service}" restart && echo "${service} restarted for new cert"
      else
        echo "Service ${service} would have been restarted if ${0} was run as root or via sudo"
      fi
    fi
  else
    echo "Error getting the service started date"
    exit 1
  fi
else
  echo "Error getting the certificate created date"
  exit 1
fi
