#!/usr/bin/env bash

# Run this script from a WordPress install directory or 
# provide the directory of a WordPress install as the 
# first argument.

# Check that the script is not being run by root
if [[ "$(id -u)" == "0" ]] ; then
  echo "You must run ${0} as the Apache / PHP-FPM user since WordPress is managed via the web interface"
  exit 1
fi

# Check if a directory is provided on the command line
if [[ "${1}" ]]; then
  if [[ -d "${1}" ]]; then
    WP_DIR="${1}"
    cd ${WP_DIR}
  else
    echo "Directory provided on the command line, ${1}, appears to not exist?"
    exit 1
  fi
else
  WP_DIR=$(pwd)
fi

# Check that wp-cli is available in a $PATH directory
WP_CLI=$(which wp)
if [[ -f "${WP_CLI}" ]]; then
  ${WP_CLI} --version | grep -q ^WP-CLI || echo "The file at ${WP_CLI} appears not to be a functioning version of wp-cli?" 
else
  echo "The WordPress command line interface was not found, check your PATH: ${PATH}"
fi

# Check if there is a WordPress install in the current directory
if [[ -f "index.php" ]]; then
  if [[ -f wp-config.php ]]; then
    WP_INSTALL=$(${WP_CLI} core is-installed && echo True || echo False) 
    if [[ "${WP_INSTALL}" == "True" ]]; then
      # Verify checksums
      ${WP_CLI} --quiet --no-color core verify-checksums
      ${WP_CLI} --quiet --no-color plugin verify-checksums --all
      # Update
      ${WP_CLI} --quiet --no-color core update
      ${WP_CLI} --quiet --no-color language core update
      ${WP_CLI} --quiet --no-color theme update --all
      ${WP_CLI} --quiet --no-color language theme update --all
      ${WP_CLI} --quiet --no-color plugin update --all
      ${WP_CLI} --quiet --no-color language plugin update --all
    else
      echo "WordPress doesn't appear to be installed in $WP_DIR"
      exit 1
    fi 
  else
    echo "No wp-config.php in $WP_DIR, provide the WordPress directory on the command line: ${0} /path/to/wp" 
  exit 1
  fi
else
  echo "No index.php found in $WP_DIR, provide the WordPress directory on the command line: ${0} /path/to/wp"
  exit 1
fi

# Clear the internal cache
${WP_CLI} --quiet --no-color cli cache clear

exit 0
