#!/bin/bash

# Fix the ownership of ~/.forward files

HOME_DIRS=$(ls /home)

for h in ${HOME_DIRS}; do
  if [[ -f /home/${h}/.forward ]]; then 
    chown ${h}:${h} /home/${h}/.forward
    chmod 600 /home/${h}/.forward
  fi
done

